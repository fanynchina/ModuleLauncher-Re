﻿namespace ModuleLauncher.Re.DataEntities.Minecraft.Network
{
    public class MojangHistoryName
    {
        public string Name { get; set; }
        public string ChangedAt { get; set; }
    }
}