﻿namespace ModuleLauncher.Re.DataEntities.Minecraft.Network
{
    public class MojangUUIDProfile
    {
        public string Name { get; set; }
        public string Uuid { get; set; }
    }
}